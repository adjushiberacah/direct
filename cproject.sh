#! /bin/bash

PROJECT_DIR=/home/virus/PycharmProjects
project_name="$1"
project_env="$1-env"
project_folder=$PROJECT_DIR/$project_name

echo "cd to $PROJECT_DIR......."
cd $PROJECT_DIR

echo "getting latest template......."
cookiecutter https://gitlab.com/gakp/service-setup.git
[[ $? -ne 0 ]] &&  exit 1
echo "creating virtual environment $project_env......."
pyenv virtualenv 3.4.4 $project_env
cd $project_folder
pyenv local $project_env
[[ -f requirements.txt ]] && pip install -r  requirements.txt
