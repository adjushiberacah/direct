import falcon
import logging

from inet.client import Client
from inet.proxy import InetClient, RoutableServer

from pylib.middleware.json import RequireJSON, JSONTranslator
from pylib import views
from pymongo import MongoClient


# configure logging for all the libraries
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('{{cookiecutter.app_name}}')


# database api
mongo_client = MongoClient()
{{cookiecutter.app_name}}_db = mongo_client.{{cookiecutter.app_name}}

# setup required middleware
api = falcon.API(middleware=[
    RequireJSON(),
    JSONTranslator()
])


# our custom error serializer
def __json_error_serializer(req, exception):
    return ('application/json', exception.to_json())
api.set_error_serializer(__json_error_serializer)


from app.{{cookiecutter.app_name}} import {{cookiecutter.app_name}}_collection, {{cookiecutter.app_name}}_entities  # noqa
views.register({{cookiecutter.app_name}}_collection, api)
views.register({{cookiecutter.app_name}}_entities, api)
