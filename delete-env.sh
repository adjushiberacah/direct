#!/bin/bash
# A script that accepts a project name as argument and deletes it
# Please rename PROJECT_DIR to match the path to your project's workspace folder 

PROJECT_DIR=/home/virus/PycharmProjects
project_name="$1"

if [ "$project_name" = "" ]; then
	echo "No project was specified!"
	echo -n "Please specify the project you intend to delete >"
	read project_name
	if [ -d "$PROJECT_DIR/$project_name" ]; then
		echo "Deleting project $project_name...."
		rm -rf "$PROJECT_DIR/$project_name"
		pyenv virtualenv-delete "$project_name-env"
		echo "$project_name Has been successfully deleted!"
	else
		echo "The specified project path $PROJECT_DIR/$project_name does not exist"
	fi
else
	if [ -d "$PROJECT_DIR/$project_name" ]; then
		echo "Deleting project $project_name...."
		rm -rf "$PROJECT_DIR/$project_name"
		pyenv virtualenv-delete "$project_name-env"
		echo "$project_name Has been successfully deleted!"
	else
		echo "The specified project path $PROJECT_DIR/$project_name does not exist"
	fi
fi
